import { users } from './UserController';

const extracts = [
  {
    sonId: 2,
    purchases: [
      {
        store: 'Cantina Ana',
        value: 1,
        when: '2019-11-26',
      },
      {
        store: 'RocketSeat store',
        value: 5,
        when: '2019-11-27',
      },
    ],
  },
];

class ExtractController {
  show(req, res) {
    const parent = users.find(u => u.id === Number(req.query.userId));

    if (parent.type !== 'parent') {
      return res.status(403).json({ error: 'Sons can not see extracts' });
    }

    const extract = extracts.find(ex => ex.sonId === parent.sonId);

    return res.json(extract.purchases);
  }
}

export default new ExtractController();
