import { questions } from './QuestionsController';

const son = {
  amount: 0,
  name: 'Lucas Moura',
  id: 1,
};

const calculate = (value, stars) => {
  if (stars === 1) {
    return value - 3;
  }

  if (stars === 2) {
    return value - 2;
  }

  return value;
};

class AnswerController {
  store(req, res) {
    const { subjectId, stars } = req.body;

    const { value } = questions.find(q => q.id === subjectId);

    const amount = calculate(value, stars);

    const newSon = {
      ...son,
      amount: son.amount + amount,
    };

    return res.json(newSon);
  }
}

export default new AnswerController();
