import { Router } from 'express';

import QuestionsController from './app/controllers/QuestionsController';
import AnswerController from './app/controllers/AnswerController';
import UserController from './app/controllers/UserController';
import ExtractController from './app/controllers/ExtractController';
import ProgressController from './app/controllers/ProgressController';

const routes = new Router();

routes.get('/users', UserController.index);
routes.get('/questions', QuestionsController.show);
routes.post('/answer', AnswerController.store);
routes.get('/progress', ProgressController.show);
routes.get('/purchases', ExtractController.show);

export default routes;
